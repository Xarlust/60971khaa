<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Все издания:</h2>

        <div class="row">
        <?php if (!empty($edition) && is_array($edition)) : ?>

            <?php foreach ($edition as $name): ?>

                <div class="col-12 col-md-6" >
                    <div class="card " style="max-width: 540px; margin: 10px;">
                        <div class="row">
                            <div class="col-md-5 d-flex align-items-center"">
                                <img height="150" src="<?= esc($name['picture_url_file']); ?>" class="card-img" alt="<?= esc($name['name']); ?>">
                                </div>

                            <div class="col-md-7">
                                <div class="card-body">
                                    <h5 class="card-title"><?= esc($name['name']); ?></h5>
                                    <p class="card-text">Автор: <?= esc($name['author']); ?></p>
                                    <a href="<?= base_url()?>/index.php/library/view/<?= esc($name['id']); ?>" class="btn btn-primary">Просмотреть</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        </div>
        <?php else : ?>
            <p>Издание не найдено.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>