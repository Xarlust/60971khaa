<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<?php if (!empty($edition)) : ?>
    <div class="container" style="max-width: 540px;">
        <?= form_open_multipart('library/update'); ?>
        <input type="hidden" name="id" value="<?= $edition["id"] ?>">

        <div class="form-group">
            <label for="name">Издание</label>
            <input id="name" type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name"
                   value="<?= $edition["name"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('name') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="author">Автор</label>
            <textarea rows="8" id="author" type="text" class="form-control <?= ($validation->hasError('author')) ? 'is-invalid' : ''; ?>" name="author"
            ><?= $edition["author"]; ?></textarea>
            <div class="invalid-feedback">
                <?= $validation->getError('author') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="picture">Изображение</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?php endif ?>
<?= $this->endSection() ?>