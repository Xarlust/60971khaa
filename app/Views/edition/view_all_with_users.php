<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php if (!empty($edition) && is_array($edition)) : ?>
            <h2>Все издания:</h2>
            <div class="d-flex justify-content-between mb-2">
                <?= $pager->links('group1','my_page') ?>
                <?php echo form_open('library/viewAllWithUsers', ['style' => 'display: flex']); ?>
                <select name="per_page" class="ml-3" aria-label="per_page">
                    <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                    <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                    <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                    <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
                </select>
                <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
                </form>
                <?= form_open('library/viewAllWithUsers',['style' => 'display: flex']); ?>
                <input type="text" class="form-control ml-3" name="search" placeholder="Имя или описание" aria-label="Search"
                       value="<?= $search; ?>">
                <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
                </form>
            </div>
            <table class="table table-striped">
                <thead>
                <th scope="col">Обложка книги</th>
                <th scope="col">Наименование</th>
                <th scope="col">Имя автора</th>
                <th scope="col">Email создателя</th>
                <th scope="col">Управление</th>
                </thead>
                <tbody>
                <?php foreach ($edition as $item): ?>
                    <tr>
                        <td>
                            <div class="col-md-5 d-flex align-items-center">
                                <img height="150" src="<?= esc($item['picture_url_file']); ?>" class="card-img" alt="<?= esc($item['name']); ?>">
                            </div>
                        </td>
                        <td><?= esc($item['name']); ?></td>
                        <td><?= esc($item['author']); ?></td>
                        <td><?= esc($item['email']); ?></td>
                        <td>
                            <a href="<?= base_url()?>/library/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                            <a href="<?= base_url()?>/library/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                            <a href="<?= base_url()?>/library/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        <?php else : ?>
            <div class="text-center">
                <p>Издание не найдено.</p>
                <a class="btn btn-dark btn-lg" href="<?= base_url()?>/library/create"><span class="fas fa-book " style="color:white"></span>&nbsp;&nbsp;Создать рейтинг</a>
            </div>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>