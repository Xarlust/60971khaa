<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php use CodeIgniter\I18n\Time; ?>
        <?php if (!empty($edition)) : ?>
            <div class="card mb-6" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-5 d-flex align-items-center">
                        <img height="150" src="<?= esc($edition['picture_url_file']); ?>" class="card-img" alt="<?= esc($edition['name']); ?>">
                    </div>
                    <div class="col-md-7">
                        <div class="card-body">
                            <h5 class="card-title">Издание:<?= esc($edition['name']); ?></h5>
                            <p class="card-text">Автор: <?= esc($edition['author']); ?></p>
                            <a class="btn btn-primary" href="<?= base_url()?>/library/edit/<?= esc($edition['id']); ?>">Редактировать</a>
                            <a class="btn btn-danger" href="<?= base_url()?>/library/delete/<?= esc($edition['id']); ?>">Удалить</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <p>Издание не найдено.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>