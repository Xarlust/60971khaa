<?php namespace App\Models;
use CodeIgniter\Model;
class LibraryModel extends Model
{
    protected $table = 'edition'; //таблица, связанная с моделью
    protected $allowedFields = ['name','author','user_id','picture_url_file'];
    public function getRating($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getLibraryWithUser($id = null,$search='')
    {
        $builder = $this->select('edition.id, edition.name, edition.author, edition.picture_url_file, email')->join('users','edition.user_id = users.id')
            ->like('name', $search,'both', null, true)
            ->orlike('author',$search,'both',null,true);
        if (!is_null($id))
        {
            return $builder->where(['edition.id' => $id])->first();
        }
        return $builder;//->findAll();
    }
}