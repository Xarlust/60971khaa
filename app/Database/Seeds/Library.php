<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Library extends Seeder
{
    public function run()
    {
        $data = [

            'name' => 'Азбука--Аттикус',
            'author' => 'ДЖ.К.Роулинг',
            //'user_id'=>1,
        ];
        $this->db->table('edition')->insert($data);

        $data = [

            'name' => 'неАзбука--Аттикус',
            'author' => 'неДЖ.К.Роулинг',
            //'user_id'=>1,
        ];
        $this->db->table('edition')->insert($data);

        $data = [

            'wear_factor' => '1',
            'id_edition' => 1,//
        ];
        $this->db->table('copy')->insert($data);


        $data = [

            'name' => 'Хрусталев Александр Алексеевич',
        ];
        $this->db->table('reader')->insert($data);
        $data = [

            'data' => '2020-12-15',
            'data_return_plan' => '2020-12-17',
            'data_return_fact' => '2020-12-16',
            'id_copy' => 1,//
            'id_reader' => 1,//
        ];
        $this->db->table('delivery')->insert($data);



    }
}