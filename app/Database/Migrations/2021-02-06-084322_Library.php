<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Library extends Migration
{
    public function up()
    {
// activity_type
        if (!$this->db->tableexists('edition')) {
// Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'author' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'user_id' => array('type' => 'INT',  'null' => TRUE),
            ));
            $this->forge->addForeignKey('user_id','users','id','RESTRICT','RESTRICT');

// create table
            $this->forge->createtable('edition', TRUE);
        }
        if (!$this->db->tableexists('copy')) {
// Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'wear_factor' => array('type' => 'INT', 'null' => FALSE),
                'id_edition' => array('type' => 'INT','unsigned' => TRUE,'null' => FALSE )
            ));
            $this->forge->addForeignKey('id_edition', 'edition', 'id', 'RESTRICT', 'RESRICT');


// create table
            $this->forge->createtable('copy', TRUE);
        }

        if (!$this->db->tableexists('reader')) {
// Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),

            ));

// create table
            $this->forge->createtable('reader', TRUE);
        }
        if (!$this->db->tableexists('delivery')) {
// Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'data' => array('type' => 'DATE','null' => FALSE),
                'data_return_plan' => array('type' => 'DATE','null' => FALSE),
                'data_return_fact' => array('type' => 'DATE','null' => FALSE),
                'id_copy' => array('type' => 'INT','unsigned' => TRUE,'null' => FALSE ),
                'id_reader' => array('type' => 'INT','unsigned' => TRUE,'null' => FALSE )

            ));
            $this->forge->addForeignKey('id_copy', 'copy', 'id', 'RESTRICT', 'RESRICT');
            $this->forge->addForeignKey('id_reader', 'reader', 'id', 'RESTRICT', 'RESRICT');

// create table
            $this->forge->createtable('delivery', TRUE);
        }


    }
//------------------------------------------------------------------—

    public function down()
    {

        $this->forge->droptable('edition');
        $this->forge->droptable('copy');
        $this->forge->droptable('reader');
        $this->forge->droptable('delivery');
    }
}
