-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Дек 22 2020 г., 16:10
-- Версия сервера: 8.0.21-0ubuntu0.20.04.4
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60971khaa`
--

-- --------------------------------------------------------

--
-- Структура таблицы `copy`
--

CREATE TABLE `copy` (
  `id` int NOT NULL,
  `wear_factor` int NOT NULL,
  `id_edition` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `copy`
--

INSERT INTO `copy` (`id`, `wear_factor`, `id_edition`) VALUES
(1, 2, 1),
(2, 4, 2),
(3, 5, 4),
(4, 1, 10),
(5, 3, 9),
(6, 3, 6),
(7, 10, 8),
(8, 7, 5),
(9, 8, 8),
(10, 8, 5),
(11, 7, 3),
(12, 3, 7);

-- --------------------------------------------------------

--
-- Структура таблицы `delivery`
--

CREATE TABLE `delivery` (
  `id` int NOT NULL,
  `data` date DEFAULT NULL,
  `data_return_plan` date DEFAULT NULL,
  `data_return_fact` date DEFAULT NULL,
  `id_copy` int NOT NULL,
  `id_reader` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `delivery`
--

INSERT INTO `delivery` (`id`, `data`, `data_return_plan`, `data_return_fact`, `id_copy`, `id_reader`) VALUES
(1, '2020-12-15', '2020-12-17', NULL, 1, 3),
(2, '2020-12-01', '2020-12-18', '2020-12-14', 2, 1),
(3, '2020-12-24', '2020-12-29', NULL, 4, 2),
(4, '2020-12-22', '2020-12-23', '2020-12-22', 3, 4),
(5, '2020-12-16', '2020-12-20', '2020-12-21', 7, 5),
(6, '2020-12-09', '2020-12-24', '2020-12-25', 6, 6),
(7, '2020-12-24', NULL, NULL, 5, 7),
(8, '2020-12-09', NULL, NULL, 8, 8),
(9, NULL, '2020-12-03', NULL, 9, 9),
(10, '2020-12-27', NULL, NULL, 10, 10),
(11, '2020-12-17', '2020-12-23', '2020-12-19', 12, 5),
(12, NULL, NULL, NULL, 11, 1),
(13, '2020-12-02', NULL, NULL, 1, 8),
(14, '2020-12-15', '2020-12-16', '2020-12-17', 6, 2),
(15, NULL, NULL, NULL, 4, 2),
(16, NULL, NULL, NULL, 2, 1),
(17, '2020-12-04', '2020-12-24', '2020-12-15', 4, 9),
(18, '2020-12-01', '2020-12-30', '2020-12-10', 12, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `edition`
--

CREATE TABLE `edition` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `edition`
--

INSERT INTO `edition` (`id`, `name`, `author`) VALUES
(1, 'Азбука-Аттикус', 'ДЖ.К.Роулинг'),
(2, 'Эксмо-АСТ', 'Дарья Доццова'),
(3, 'РИПОЛ классик', 'Стина Джексон'),
(4, 'РИПОЛ классик', 'Мелани Жюсти'),
(5, 'Эксмо-АСТ', 'Анна Литвинова'),
(6, 'Азбука-Аттикус', 'Кевин Кван'),
(7, 'Росмэн', 'Аксанов Сергей Тимофеевич'),
(8, 'Росмэн', 'Бажов Павел Петрович'),
(9, 'Феникс', 'Лавкрафт'),
(10, 'Феникс', 'Грин'),
(11, 'Эксмо', 'Эмили Барр'),
(12, 'Эксмо', 'Ник Перумов');

-- --------------------------------------------------------

--
-- Структура таблицы `reader`
--

CREATE TABLE `reader` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `reader`
--

INSERT INTO `reader` (`id`, `name`) VALUES
(1, 'Хрусталев Александр Алексеевич'),
(2, 'Осипов Антон Олегович'),
(3, 'Алиев Вусал Оглы'),
(4, 'Семенов Евгений'),
(5, 'Шевчук Анна'),
(6, 'Василенко Сергей'),
(7, 'Баженова Анна'),
(8, 'Корепанов Андрей'),
(9, 'Арсений Малясов'),
(10, 'Андрей Овсяный');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `copy`
--
ALTER TABLE `copy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `copy_ibfk_1` (`id_edition`);

--
-- Индексы таблицы `delivery`
--
ALTER TABLE `delivery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_copy` (`id_copy`),
  ADD KEY `id_reader` (`id_reader`);

--
-- Индексы таблицы `edition`
--
ALTER TABLE `edition`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reader`
--
ALTER TABLE `reader`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `copy`
--
ALTER TABLE `copy`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `delivery`
--
ALTER TABLE `delivery`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `edition`
--
ALTER TABLE `edition`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `reader`
--
ALTER TABLE `reader`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `copy`
--
ALTER TABLE `copy`
  ADD CONSTRAINT `copy_ibfk_1` FOREIGN KEY (`id_edition`) REFERENCES `edition` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `delivery`
--
ALTER TABLE `delivery`
  ADD CONSTRAINT `delivery_ibfk_1` FOREIGN KEY (`id_copy`) REFERENCES `copy` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `delivery_ibfk_2` FOREIGN KEY (`id_reader`) REFERENCES `reader` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
